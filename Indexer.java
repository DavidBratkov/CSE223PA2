/*
By: David Bratkov

Class: CSE223

Date: 5/8/2020

Description: This is a class that will take a file as a input and then scan the entire file for words and then translates them to upper case and then stores each word in a hashmap that consists of linkedlists of each place when that word appeared in the file.

*/
import java.util.Scanner; //required to use the Scanner class
import java.io.File; //Needed for the Scanner because it is 
import java.util.HashMap; //Required to store all the linked list of words in
import java.util.LinkedList; //Required for the HashMap

public class Indexer{

private boolean flag;

HashMap<String, LinkedList<Integer>> Index = new HashMap<String, LinkedList<Integer>>();//Declaring the Hashmap and how its going to layed out


//This is the main method, It takes in a file and then splits it up into lines and then into words, then it fixes up the words (ie.getting rid of all the punctuation, and makes them all capital case) then adds it to the hashmap
public boolean processFile(String filename){

int loc = 0;//variable used to keep track of the location in the file

Scanner scan = null;//declared the scanner outside of the try catch block to be able to close it after it

	try{
			
		File filenew = new File(filename);
			
		scan = new Scanner(filenew);
		
		while(scan.hasNext()){ // This loops untill there is nothing left in the file
			
			String input = scan.next();
				
			//String[] words = input.split(" ");
	
			String temp1 = cleanupWord(input);

			if(temp1.equals("") == false){ //checks if the string temp1 does not equal an empty string
			
				loc++;				

				addReference(temp1,loc);

			}
			
		}
	
	}	 
	
	catch(Exception error){ //This catches any errors that happen in the try block

		flag = false;

		return(false);
		
	
	}

flag = true;

scan.close();//closes the scan

return(true);

}

//This is a simple method that looks at the amound of linked list nodes there are related to the given word
public int numberOfInstances(String word){

	if(flag == false){

		return(-1);

	}

	if(Index.containsKey(word)){//checks to see if the hashmap has the specific word

		return(Index.get(word).size());

	}	

return(0);

}

//This method takes in a word and the instance number and tries to see if there is a node related to it and then returns the value stores in the node
public int locationOf(String word, int instanceNum){

         if(flag == false || (Index.get(word) == null) || instanceNum < 0 || instanceNum >= numberOfInstances(word)){

                 return(-1);

         }

return(Index.get(word).get(instanceNum));//goes through the index and finds the word and then gets the specific node and returns the value	
				
}

//This method just returns an int showing how many words it found 
public int numberOfWords(){

         if(flag == false){

                 return(-1);

         }
	
return(Index.size());

}

//This is needed because of the private class this is needed to print the toString method
public String toString(){

	if(flag == false){
	
		return(null);

	}

return(Index.toString());

}

//This method when called would recive a word and then scan a word with the charAt and replace it nothing else then characters and then returns a upper case of the fixed word
private String cleanupWord(String word){

String fixed = new String();

int x=0;

	for(int i=0;i<word.length();i++){
	
	
		if(Character.isLetter(word.charAt(i))) { //This was a tricky statment to get to work but I finally got it work by getting the isletter of the character that word charAt returns

			fixed = (fixed + word.charAt(i));

			x++;

		}

	}
	
return(fixed.toUpperCase());

}


//This method when called will search the hashmap for the word and if it finds it, then it will add the location but if it isnt found then it would create a new entry
private void addReference(String word, int location){

	if(word.equals("")){
	
		return;

	}
				
	if(Index.containsKey(word)){
				
		Index.get(word).add(location);
			
	}		

	//if(Index.get(word) == null){

				
		LinkedList<Integer> temp = new LinkedList<Integer>();//Needed to make a linked list to make the put work 

		temp.add(location);

		//Index.put(word,temp);
	
		Index.putIfAbsent(word,temp); //Made this work!
				
	//}				
}				
				



}//closing of the class
